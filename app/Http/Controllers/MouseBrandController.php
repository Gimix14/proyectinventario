<?php

namespace App\Http\Controllers;

use App\Models\MouseBrand;
use App\Http\Requests\StoreMouseBrandRequest;
use App\Http\Requests\UpdateMouseBrandRequest;

class MouseBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMouseBrandRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(MouseBrand $mouseBrand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(MouseBrand $mouseBrand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMouseBrandRequest $request, MouseBrand $mouseBrand)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(MouseBrand $mouseBrand)
    {
        //
    }
}
